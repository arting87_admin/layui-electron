const ipc = require('electron').ipcRenderer
const shell = require('electron').shell
const path = require('path')
const url = require('url')
const Store = require('electron-store');
const store = new Store();

layui.use(['element', 'form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块

    function resizeWebSize() {
        $(window).on('resize', function () {
            var currBoxHeight = $('.layui-body .layui-tab-content').height(); //获取当前容器的高度
            var currBoxWidth = $('.layui-body .layui-tab-content').width(); //获取当前容器的高度
            $('iframe').height(currBoxHeight);
            $('iframe').width(currBoxWidth + 10);
            // $('object').height(currBoxHeight);
            // $('object').width(currBoxWidth);
        }).resize();
    }

    resizeWebSize();

});









